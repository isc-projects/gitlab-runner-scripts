# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: 0BSD
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
# SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
# IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

"""
executor.py - GitLab Runner Custom executor script for Docker on Windows
"""

import json
import os
import subprocess
import sys


DOCKER_PATH = "C:\\Program Files\\Docker\\docker.exe"


def fatal_error(message):
    """
    Print an error message to stderr and exit.
    """
    print(message, file=sys.stderr)
    sys.exit(1)


def docker_command(cmd):
    """
    Invoke the given Docker command, exiting immediately if it fails.
    """
    try:
        subprocess.run([DOCKER_PATH] + cmd, check=True)
    except subprocess.CalledProcessError:
        sys.exit(1)


def do_config(*_):
    """
    Perform the Config stage of a GitLab CI job.
    """
    config = {
        "builds_dir": "C:\\builds",
        "cache_dir": "C:\\cache",
        "builds_dir_is_shared": False,
    }
    print(json.dumps(config))


def do_prepare(container_name, _):
    """
    Perform the Prepare stage of a GitLab CI job.
    """
    docker_command(
        [
            "run",
            "--name",
            container_name,
            "--interactive",
            "--detach",
            "--rm",
            os.environ["CUSTOM_ENV_CI_JOB_IMAGE"],
        ]
    )


def do_run(container_name, args):
    """
    Perform the Run stage of a GitLab CI job.
    """
    valid_stages = [
        "prepare_script",
        "get_sources",
        "restore_cache",
        "download_artifacts",
        "build_script",
        "step_script",
        "after_script",
        "archive_cache",
        "upload_artifacts_on_success",
        "upload_artifacts_on_failure",
        "cleanup_file_variables",
    ]

    script_src = args[0]
    stage = args[1]

    if stage not in valid_stages:
        fatal_error(f'Unsupported run stage "{stage}"')

    script_dst_path = f"C:\\{stage}.ps1"
    script_dst = f"{container_name}:{script_dst_path}"
    docker_command(["cp", script_src, script_dst])
    docker_command(["exec", container_name, "powershell.exe", script_dst_path])


def do_cleanup(container_name, _):
    """
    Perform the Cleanup stage of a GitLab CI job.
    """
    docker_command(["stop", container_name])


def main():
    """
    Parse arguments and perform the requested stage of a GitLab CI job.
    """
    actions = {
        "config": do_config,
        "prepare": do_prepare,
        "run": do_run,
        "cleanup": do_cleanup,
    }
    valid_actions = "|".join(actions.keys())

    try:
        action = actions[sys.argv[1]]
    except (IndexError, KeyError):
        fatal_error(f"Usage: {sys.argv[0]} {valid_actions} [args...]")

    container_name = "gitlab-runner-" + os.environ["CUSTOM_ENV_CI_JOB_ID"]
    action(container_name, sys.argv[2:])


if __name__ == "__main__":
    main()
