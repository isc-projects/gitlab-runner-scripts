<!--
Copyright (C) Internet Systems Consortium, Inc. ("ISC")

SPDX-License-Identifier: 0BSD

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
-->

# GitLab Runner Custom Executor for Docker on Windows

## Overview

This Python script allows GitLab CI jobs to be run inside Docker
containers on Windows Server systems which are [not supported][1] by the
native Docker executor included in GitLab Runner.

## Prerequisites

- [GitLab Runner][2]
- [Docker][3]
- [Python 3.6+][4]

## Installation

- Register a new GitLab CI runner using `gitlab-runner.exe register`,
  specifying `custom` as the executor to use.

- Adjust the relevant section of GitLab Runner's `config.toml` so
  that the executor gets invoked properly:

  ```toml
  shell = "powershell"
  [runners.custom]
    config_exec = "C:\\path\\to\\python\\python.exe"
    config_args = [ "C:\\path\\to\\windows-docker\\executor.py", "config" ]
    prepare_exec = "C:\\path\\to\\python\\python.exe"
    prepare_args = [ "C:\\path\\to\\windows-docker\\executor.py", "prepare" ]
    run_exec = "C:\\path\\to\\python\\python.exe"
    run_args = [ "C:\\path\\to\\windows-docker\\executor.py", "run" ]
    cleanup_exec = "C:\\path\\to\\python\\python.exe"
    cleanup_args = [ "C:\\path\\to\\windows-docker\\executor.py", "cleanup" ]
  ```

## Limitations

- Docker images are *not* automatically pulled or updated to the latest
  available version during job setup.  `docker pull` needs to be either
  run manually or from the Windows Task Scheduler.

[1]: https://docs.gitlab.com/runner/executors/docker.html#supported-windows-versions
[2]: https://docs.gitlab.com/runner/install/windows.html
[3]: https://docs.microsoft.com/en-us/virtualization/windowscontainers/quick-start/set-up-environment
[4]: https://www.python.org/downloads/windows/
