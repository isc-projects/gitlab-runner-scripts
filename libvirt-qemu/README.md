<!--
Copyright (C) Internet Systems Consortium, Inc. ("ISC")

SPDX-License-Identifier: 0BSD

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
-->

# GitLab Runner Custom Executor for QEMU-based libvirt Domains

## Overview

This shell script allows GitLab CI jobs to be run inside QEMU-based
libvirt domains (over SSH).

## Required Tools

- `arp`
- `qemu-img`
- `ssh-keyscan`
- `sshpass`
- `virsh`

## Installation

- Clone this repository to the libvirt host which will be running GitLab
  CI jobs.

- Copy the sample configuration file (`executor.conf.sample`) to
  `/etc/gitlab-runner/executor-libvirt-qemu.conf`.

- Adjust configuration file contents to match your preferences.  In
  particular, set `BASE_DIR` to the path to the executor's working
  directory where all temporary files will be stored.

- Adjust the libvirt domain template (a sample is provided as
  `domain-template.xml`) to match your preferences.

- Put the operating system QCOW2 images to be used by the executor in
  the `${BASE_DIR}/qcow2` directory.

- Register a new GitLab CI runner using `gitlab-runner register`,
  specifying `custom` as the executor to use.

- Adjust the relevant section of `/etc/gitlab-runner/config.toml` so
  that the executor gets invoked properly:

  ```toml
  [runners.custom]
    config_exec = "/path/to/libvirt-qemu/executor.sh"
    config_args = ["config"]
    prepare_exec = "/path/to/libvirt-qemu/executor.sh"
    prepare_args = ["prepare"]
    run_exec = "/path/to/libvirt-qemu/executor.sh"
    run_args = ["run"]
    cleanup_exec = "/path/to/libvirt-qemu/executor.sh"
    cleanup_args = ["cleanup"]
  ```

- In `/etc/gitlab-runner/config.toml`, set the `limit` [runner
  variable][1] to the maximum number of GitLab CI jobs to run (i.e.
  virtual machines to create) simultaneously at any given time, based on
  the amount of memory available on your host and the load it is capable
  of handling.

## Design

- A separate libvirt domain is created for each GitLab CI job.  A
  common XML template is used as a base for all domain definitions.

- Every supported operating system has a corresponding QCOW2 image.
  These images are *not* prepared by the executor script.  For some
  ideas on how to do this, see the `packer` directory in the [ISC
  images][2] Git repository.

- For each GitLab CI job, a separate QCOW2 image is created with the
  given operating system's QCOW2 image specified as the backing file
  (`qemu-img create -b ...`).  This speeds up domain creation and saves
  disk space as it is *not* necessary to copy the whole QCOW2 image
  before starting each virtual machine.

- Every created domain is assigned a single virtual network interface.
  The `default` libvirt network needs to be started before using the
  executor script.

- The IP address assigned to the given domain is determined by looking
  up its MAC address in the output of the `arp` tool.

- Job scripts are executed by connecting to virtual machines using
  password-based SSH.  Access credentials are configured by setting the
  `SSH_USER` and `SSH_PASSWORD` configuration variables appropriately.
  The username can be overridden on a per-job basis by setting the
  `USER` variable in a given job's YAML definition.

- While not strictly necessary, each domain is expected to use the first
  serial port (COM1) as its system console.  The sample domain template
  redirects COM1 to a file which can be inspected on the host in case of
  boot issues.

## QCOW2 Image Synchronization

Whenever [GitLab Runner Docker executor][3] runs a GitLab CI job, it
first checks whether a newer version of the Docker image to be used is
available; if so, the latest version of the image is automatically
downloaded, used, and cached locally.  No similar mechanisms are built
into [GitLab Runner Custom executor][4].

Since GitLab comes with a built-in [Container Registry][5] and
maintaining a separate infrastructure just for hosting QCOW2 images
would be cumbersome, for the time being, ISC uses a simple trick to
solve the image distribution issue: the QCOW2 images built are wrapped
into Docker containers and distributed through the GitLab Container
Registry.

However, this still does not cause QCOW2 images to be updated
automatically whenever a GitLab CI job is run.  As implementing such a
mechanism in a safe manner is a non-trivial task and we generally update
our QCOW2 images only when a new version of a given operating system is
released (i.e. rarely), our GitLab CI runners execute a rather crude
helper script (`pull-images.sh`) as an hourly cron job.  This script
pulls the latest Docker images available from the specified Docker
repository, extracts the QCOW2 images embedded in them and replaces the
locally stored QCOW2 images with their up-to-date versions.  All QCOW2
images are expected to be published in the same Docker repository,
specified using the `DOCKER_REPOSITORY` variable in the executor's
configuration file.

The biggest caveat with this approach is that removing a QCOW2 backing
file while it is used by a libvirt domain is obviously more than likely
to cause a given GitLab CI job to fail.  Yet, given the low frequency of
QCOW2 image updates, this approach works for us for the time being.
Running the script on an hourly basis on all runners ensures they pull
updated QCOW2 images automatically and that it happens relatively
quickly after these updated QCOW2 images are published.  The helper
script can also be run manually before the first use of the executor in
order to download all QCOW2 images available in the specified Docker
repository to `${BASE_DIR}/qcow2`.

[1]: https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-runners-section
[2]: https://gitlab.isc.org/isc-projects/images/-/tree/master/packer
[3]: https://docs.gitlab.com/runner/executors/docker.html
[4]: https://docs.gitlab.com/runner/executors/custom.html
[5]: https://docs.gitlab.com/ce/user/packages/container_registry/index.html
