#!/bin/sh

# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: 0BSD
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
# SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
# IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#
# executor.sh - GitLab Runner Custom Executor for QEMU-based libvirt Domains
#

set -e

CONFIG_PATH="/etc/gitlab-runner/executor-libvirt-qemu.conf"

# shellcheck source=executor.conf.sample
. "${CONFIG_PATH}"

BASE_IMAGE_DIR="${BASE_DIR}/qcow2"
RUNNER_NAME="runner-${CUSTOM_ENV_CI_JOB_ID}"
DOMAIN_DEFINITION_PATH="${BASE_DIR}/${RUNNER_NAME}.xml"
DOMAIN_LOG_PATH="${BASE_DIR}/${RUNNER_NAME}.log"
IMAGE_PATH="${BASE_DIR}/${RUNNER_NAME}.qcow2"
SSH_KNOWN_HOSTS_PATH="${BASE_DIR}/${RUNNER_NAME}.pub"

fatal_error() {
	echo "${1}" >&2
	exit 1
}

get_runner_ip() {
	MAC_ADDRESS="$(virsh dumpxml "${RUNNER_NAME}" | grep -oE "[0-9a-f:]{17}")"
	arp -n | awk '$3 == "'"${MAC_ADDRESS}"'" { print $1 }'
}

do_config() {
	cat <<-EOF
	{
		"builds_dir": "/builds",
		"cache_dir": "/tmp",
		"builds_dir_is_shared": false
	}
	EOF
}

do_prepare() {
	BASE_IMAGE_PATH="$(readlink -f "${BASE_IMAGE_DIR}/${CUSTOM_ENV_CI_JOB_IMAGE}")"
	if [ ! -f "${BASE_IMAGE_PATH}" ]; then
		fatal_error "Base image ${CUSTOM_ENV_CI_JOB_IMAGE} does not exist"
	fi

	cp "${DOMAIN_DEFINITION_TEMPLATE_PATH}" "${DOMAIN_DEFINITION_PATH}"
	for TOKEN_NAME in DOMAIN_LOG_PATH IMAGE_PATH RUNNER_NAME; do
		eval "TOKEN_VALUE=\"\$${TOKEN_NAME}\""
		sed -i "s|@${TOKEN_NAME}@|${TOKEN_VALUE}|g" "${DOMAIN_DEFINITION_PATH}"
	done

	qemu-img create -f qcow2 -F qcow2 -b "${BASE_IMAGE_PATH}" "${IMAGE_PATH}"

	virsh define "${DOMAIN_DEFINITION_PATH}"
	virsh start "${RUNNER_NAME}"

	BOOT_OK=0
	BOOT_START="$(date +%s)"
	while [ "$(($(date +%s) - BOOT_START))" -lt "${BOOT_TIMEOUT}" ]; do
		IPV4_ADDRESS="$(get_runner_ip)"
		if [ -z "${IPV4_ADDRESS}" ]; then
			sleep 1
			continue
		fi
		if [ -z "$(ssh-keyscan -T 1 "${IPV4_ADDRESS}" 2>/dev/null)" ]; then
			sleep 1
			continue
		fi
		BOOT_OK=1
		break
	done
	if [ "${BOOT_OK}" -eq 0 ]; then
		cp "${DOMAIN_LOG_PATH}" "${DOMAIN_LOG_PATH}.boot-timeout"
		fatal_error "VM boot timed out"
	fi
}

do_run() {
	SCRIPT_PATH="${1}"
	RUN_STAGE="${2}"

	case "${RUN_STAGE}" in
		"prepare_script") ;;
		"get_sources") ;;
		"restore_cache") ;;
		"download_artifacts") ;;
		"build_script") ;;
		"step_script") ;;
		"after_script") ;;
		"archive_cache") ;;
		"upload_artifacts_on_success"|"upload_artifacts_on_failure") ;;
		"cleanup_file_variables") ;;
		*) fatal_error "Unsupported run stage '${RUN_STAGE}'" ;;
	esac

	IPV4_ADDRESS="$(get_runner_ip)"

	if [ -n "${CUSTOM_ENV_USER}" ]; then
		SSH_USER="${CUSTOM_ENV_USER}"
	fi

	sshpass -p "${SSH_PASSWORD}" ssh			\
		-o "StrictHostKeyChecking=no"			\
		-o "UserKnownHostsFile=${SSH_KNOWN_HOSTS_PATH}"	\
		"${SSH_USER}@${IPV4_ADDRESS}"			\
		"bash" < "${SCRIPT_PATH}"
}

do_cleanup() {
	virsh destroy "${RUNNER_NAME}"
	virsh undefine "${RUNNER_NAME}"

	rm -f "${DOMAIN_DEFINITION_PATH}"
	rm -f "${DOMAIN_LOG_PATH}"
	rm -f "${IMAGE_PATH}"
	rm -f "${SSH_KNOWN_HOSTS_PATH}"
}

ensure_variable_set() {
	eval VALUE="\${${1}+set}"
	if [ -z "${VALUE}" ]; then
		fatal_error "${1} not set, please check contents of ${CONFIG_PATH}"
	fi
}

ensure_program_available() {
	if ! command -v "${1}" > /dev/null 2>&1; then
		fatal_error "'${1}' not found in PATH"
	fi
}

ensure_variable_set BASE_DIR
ensure_variable_set BOOT_TIMEOUT
ensure_variable_set DOMAIN_DEFINITION_TEMPLATE_PATH
ensure_variable_set SSH_PASSWORD
ensure_variable_set SSH_USER

ensure_program_available arp
ensure_program_available qemu-img
ensure_program_available ssh-keyscan
ensure_program_available sshpass
ensure_program_available virsh

MODE="${1}"

case "${MODE}" in
	"config") ;;
	"prepare") ;;
	"run") ;;
	"cleanup") ;;
	*) fatal_error "Usage: $0 config|prepare|run|cleanup [args...]" ;;
esac

shift
"do_${MODE}" "$@"
