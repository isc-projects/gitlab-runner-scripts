#!/bin/sh

# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: 0BSD
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
# SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
# IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#
# pull-images.sh - Pull QCOW2 Images Embedded in Docker Images From a Registry
#

set -e

CONFIG_PATH="/etc/gitlab-runner/executor-libvirt-qemu.conf"

# shellcheck source=executor.conf.sample
. "${CONFIG_PATH}"

fatal_error() {
	echo "${1}" >&2
	exit 1
}

ensure_variable_set() {
	eval VALUE="\${${1}+set}"
	if [ -z "${VALUE}" ]; then
		fatal_error "${1} not set, please check contents of ${CONFIG_PATH}"
	fi
}

ensure_variable_set BASE_DIR
ensure_variable_set DOCKER_REPOSITORY

if ! command -v "docker" > /dev/null 2>&1; then
	fatal_error "docker not available, aborting"
fi

BASE_IMAGE_DIR="${BASE_DIR}/qcow2"

get_local_qcow2_image_list() {
	docker image ls "${DOCKER_REPOSITORY}" | tail -n +2 | awk '$2 != "<none>" { print $2 "=" $3 }' | sort
}

mkdir -p "${BASE_IMAGE_DIR}"
ORIGINAL_IMAGES="$(mktemp)"
LATEST_IMAGES="$(mktemp)"
get_local_qcow2_image_list > "${ORIGINAL_IMAGES}"
docker pull -a "${DOCKER_REPOSITORY}"
get_local_qcow2_image_list > "${LATEST_IMAGES}"
diff -u "${ORIGINAL_IMAGES}" "${LATEST_IMAGES}" | sed -nE "s|^\+([^+=][^=]*)=([0-9a-f]+$)|\1 \2|p" | while read -r QCOW2_IMAGE VERSION; do
	CONTAINER_ID="$(docker create "${DOCKER_REPOSITORY}:${QCOW2_IMAGE}" /bin/sh)"
	docker cp "${CONTAINER_ID}:${QCOW2_IMAGE}.qcow2" "${BASE_IMAGE_DIR}/${QCOW2_IMAGE}-${VERSION}.qcow2"
	docker rm "${CONTAINER_ID}"
	ln -sf "${QCOW2_IMAGE}-${VERSION}.qcow2" "${BASE_IMAGE_DIR}/${QCOW2_IMAGE}"
done
rm -f "${ORIGINAL_IMAGES}" "${LATEST_IMAGES}"
docker image ls | awk '$1 == "'"${DOCKER_REPOSITORY}"'" && $2 == "<none>" { print $3 }' | while read -r IMAGE_ID; do
	find "${BASE_IMAGE_DIR}" -type f -name "*-${IMAGE_ID}.qcow2" -delete
done
