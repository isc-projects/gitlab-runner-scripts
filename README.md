<!--
Copyright (C) Internet Systems Consortium, Inc. ("ISC")

SPDX-License-Identifier: 0BSD

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
-->

# GitLab Runner Custom Executor Scripts

## Overview

This repository holds [GitLab Runner Custom executor][1] scripts which
the [Internet Systems Consortium][2] uses with [GitLab CI][3] for
testing its products in environments which are not natively supported by
[GitLab Runner][4].

[1]: https://docs.gitlab.com/runner/executors/custom.html
[2]: https://www.isc.org/
[3]: https://docs.gitlab.com/ce/ci/
[4]: https://docs.gitlab.com/runner/
